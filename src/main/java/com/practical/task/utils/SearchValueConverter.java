package com.practical.task.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.practical.task.beans.SearchValueBean;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;

/**
 * Created by Denis Dulyak on 02.11.2016.
 */
@Converter
public class SearchValueConverter implements AttributeConverter<SearchValueBean, String> {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(SearchValueBean attribute) {
        String jsonString = "";
        try {
            jsonString = objectMapper.writeValueAsString(attribute);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }
        return jsonString;
    }

    @Override
    public SearchValueBean convertToEntityAttribute(String dbData) {
        SearchValueBean valueBean = new SearchValueBean();
        try {
            valueBean = objectMapper.readValue(dbData, SearchValueBean.class);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return valueBean;
    }
}
