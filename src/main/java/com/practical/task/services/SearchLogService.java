package com.practical.task.services;

import com.practical.task.beans.SearchValueBean;
import com.practical.task.entities.SearchLog;
import com.practical.task.repositories.SearchLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by Denis Dulyak on 02.11.2016.
 */
@Service
public class SearchLogService {

    @Autowired
    private SearchLogRepository repository;

    public Page<SearchLog> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public SearchLog addLog(SearchValueBean valueBean, String fbUserId, Integer count) {
        SearchLog log = new SearchLog();

        log.setFbUserId(fbUserId);
        log.setCreateDate(new Date());
        log.setSearchValue(valueBean);
        log.setCount(count);

        return repository.save(log);
    }
}
