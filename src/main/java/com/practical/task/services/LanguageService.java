package com.practical.task.services;

import com.practical.task.entities.Language;
import com.practical.task.repositories.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by Denis Dulyak on 03.11.2016.
 */
@Service
public class LanguageService {

    @Autowired
    private LanguageRepository repository;

    public Page<Language> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }
}
