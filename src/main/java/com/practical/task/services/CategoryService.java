package com.practical.task.services;

import com.practical.task.entities.Category;
import com.practical.task.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by Denis Dulyak on 03.11.2016.
 */
@Service
public class CategoryService {

    @Autowired
    private CategoryRepository repository;

    public Page<Category> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }
}
