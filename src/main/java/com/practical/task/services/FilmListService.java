package com.practical.task.services;

import com.practical.task.beans.SearchValueBean;
import com.practical.task.entities.views.FilmList;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denis Dulyak on 02.11.2016.
 */
@Service
public class FilmListService {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private SearchLogService logService;

    public List<FilmList> search(SearchValueBean valueBean, String fbUserId) {
        List<FilmList> filmLists = entityManager.createQuery(buildSearchQuery(valueBean), FilmList.class).getResultList();

        logService.addLog(valueBean, fbUserId, filmLists.size());
        return filmLists;
    }

    private String buildSearchQuery(SearchValueBean valueBean) {
        String query = "select f from FilmList f ";

        List<String> values = new ArrayList<>();
        if (StringUtils.isNotEmpty(valueBean.getTitle())) {
            values.add(" f.title like '%" + valueBean.getTitle() + "%' ");
        }
        if (StringUtils.isNotEmpty(valueBean.getDescription())) {
            values.add(" f.description like '%" + valueBean.getDescription() + "%' ");
        }
        if (StringUtils.isNotEmpty(valueBean.getCategory())) {
            values.add(" f.category like '%" + valueBean.getCategory() + "%' ");
        }
        if (StringUtils.isNotEmpty(valueBean.getActor())) {
            values.add(" f.actors like '%" + valueBean.getActor() + "%' ");
        }
        if (StringUtils.isNotEmpty(valueBean.getLanguageName())) {
            values.add(" f.languageName like '%" + valueBean.getLanguageName() + "%' ");
        }

        if (values.isEmpty()) {
            return query;
        }

        return query + " where " + StringUtils.join(values, " and ");
    }
}
