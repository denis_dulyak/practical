package com.practical.task.services;

import com.practical.task.entities.Actor;
import com.practical.task.repositories.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by Denis Dulyak on 03.11.2016.
 */
@Service
public class ActorService {

    @Autowired
    private ActorRepository repository;

    public Page<Actor> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }
}
