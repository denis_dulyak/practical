package com.practical.task.controllers;

import com.practical.task.entities.Category;
import com.practical.task.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Denis Dulyak on 03.11.2016.
 */
@RestController
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public Page<Category> find(@RequestParam Integer page, @RequestParam Integer size) {
        return categoryService.findAll(new PageRequest(page, size));
    }
}
