package com.practical.task.controllers;

import com.practical.task.beans.SearchValueBean;
import com.practical.task.entities.views.FilmList;
import com.practical.task.services.FilmListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Denis Dulyak on 01.11.2016.
 */
@RestController
@RequestMapping("/api/search")
public class SearchController {

    @Autowired
    private FilmListService filmListService;

    @RequestMapping(method = RequestMethod.GET)
    public List<FilmList> search(@ModelAttribute SearchValueBean valueBean) {
        OAuth2Authentication auth = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        return filmListService.search(valueBean, auth.getPrincipal() + "");
    }
}
