package com.practical.task.controllers;

import com.practical.task.entities.Language;
import com.practical.task.services.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Denis Dulyak on 03.11.2016.
 */
@RestController
@RequestMapping("/api/language")
public class LanguageController {

    @Autowired
    private LanguageService languageService;

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public Page<Language> find(@RequestParam Integer page, @RequestParam Integer size) {
        return languageService.findAll(new PageRequest(page, size));
    }
}
