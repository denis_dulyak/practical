package com.practical.task.controllers;

import com.practical.task.entities.SearchLog;
import com.practical.task.services.SearchLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Denis Dulyak on 02.11.2016.
 */
@RestController
@RequestMapping("/api/log")
public class LogController {

    @Autowired
    private SearchLogService logService;

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public Page<SearchLog> find(@RequestParam Integer page, @RequestParam Integer size) {
        return logService.findAll(new PageRequest(page, size));
    }
}
