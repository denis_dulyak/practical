package com.practical.task.controllers;

import com.practical.task.entities.Actor;
import com.practical.task.services.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Denis Dulyak on 03.11.2016.
 */
@RestController
@RequestMapping("/api/actor")
public class ActorController {

    @Autowired
    private ActorService actorService;

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public Page<Actor> find(@RequestParam Integer page, @RequestParam Integer size) {
        return actorService.findAll(new PageRequest(page, size));
    }
}
