package com.practical.task.repositories;

import com.practical.task.entities.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Denis Dulyak on 03.11.2016.
 */
@Repository
public interface LanguageRepository extends JpaRepository<Language, Byte> {
}
