package com.practical.task.repositories;

import com.practical.task.entities.views.FilmList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Denis Dulyak on 02.11.2016.
 */
@Repository
public interface FilmListRepository extends JpaRepository<FilmList, Short> {
}
