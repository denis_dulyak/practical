package com.practical.task.repositories;

import com.practical.task.entities.SearchLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Denis Dulyak on 02.11.2016.
 */
@Repository
public interface SearchLogRepository extends JpaRepository<SearchLog, Long> {
}
