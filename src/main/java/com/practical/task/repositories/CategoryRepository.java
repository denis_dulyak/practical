package com.practical.task.repositories;

import com.practical.task.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Denis Dulyak on 03.11.2016.
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Byte> {
}
