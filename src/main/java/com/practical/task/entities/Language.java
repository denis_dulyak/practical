package com.practical.task.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Table(name = "language", catalog = "sakila")
public class Language implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "language_id", unique = true, nullable = false)
	private Byte languageId;

	@Column(name = "name", nullable = false, length = 20)
	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update", nullable = false)
	private Date lastUpdate;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "languageByLanguageId")
	private List<Film> filmsForLanguageId;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "languageByOriginalLanguageId")
	private List<Film> filmsForOriginalLanguageId;
}
