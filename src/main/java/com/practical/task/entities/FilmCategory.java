package com.practical.task.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "film_category", catalog = "sakila")
public class FilmCategory implements java.io.Serializable {

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "filmId", column = @Column(name = "film_id", nullable = false)),
		@AttributeOverride(name = "categoryId", column = @Column(name = "category_id", nullable = false))
	})
	private FilmCategoryId id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "category_id", nullable = false, insertable = false, updatable = false)
	private Category category;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "film_id", nullable = false, insertable = false, updatable = false)
	private Film film;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update", nullable = false)
	private Date lastUpdate;
}
