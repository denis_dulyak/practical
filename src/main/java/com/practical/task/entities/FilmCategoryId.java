package com.practical.task.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Setter
@Embeddable
public class FilmCategoryId implements java.io.Serializable {

	@Column(name = "film_id", nullable = false)
	private short filmId;

	@Column(name = "category_id", nullable = false)
	private byte categoryId;

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof FilmCategoryId))
			return false;
		FilmCategoryId castOther = (FilmCategoryId) other;

		return (this.getFilmId() == castOther.getFilmId())
				&& (this.getCategoryId() == castOther.getCategoryId());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getFilmId();
		result = 37 * result + this.getCategoryId();
		return result;
	}
}
