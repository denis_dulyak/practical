package com.practical.task.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Table(name = "store", catalog = "sakila", uniqueConstraints = @UniqueConstraint(columnNames = "manager_staff_id"))
public class Store implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "store_id", unique = true, nullable = false)
	private Byte storeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "address_id", nullable = false)
	private Address address;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "manager_staff_id", unique = true, nullable = false)
	private Staff staff;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update", nullable = false)
	private Date lastUpdate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	private List<Staff> staffs;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	private List<Inventory> inventories;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	private List<Customer> customers;
}
