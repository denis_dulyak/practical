package com.practical.task.entities.views;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Getter
@Setter
@Embeddable
public class SalesByStoreId implements java.io.Serializable {

	@Column(name = "store", length = 101)
	private String store;

	@Column(name = "manager", length = 91)
	private String manager;

	@Column(name = "total_sales", precision = 27)
	private BigDecimal totalSales;

	public SalesByStoreId() {
	}

	public SalesByStoreId(String store, String manager, BigDecimal totalSales) {
		this.store = store;
		this.manager = manager;
		this.totalSales = totalSales;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SalesByStoreId))
			return false;
		SalesByStoreId castOther = (SalesByStoreId) other;

		return ((this.getStore() == castOther.getStore()) || (this.getStore() != null
				&& castOther.getStore() != null && this.getStore().equals(
				castOther.getStore())))
				&& ((this.getManager() == castOther.getManager()) || (this
						.getManager() != null && castOther.getManager() != null && this
						.getManager().equals(castOther.getManager())))
				&& ((this.getTotalSales() == castOther.getTotalSales()) || (this
						.getTotalSales() != null
						&& castOther.getTotalSales() != null && this
						.getTotalSales().equals(castOther.getTotalSales())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getStore() == null ? 0 : this.getStore().hashCode());
		result = 37 * result + (getManager() == null ? 0 : this.getManager().hashCode());
		result = 37 * result + (getTotalSales() == null ? 0 : this.getTotalSales().hashCode());

		return result;
	}
}
