package com.practical.task.entities.views;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Setter
@Embeddable
public class ActorInfoId implements java.io.Serializable {

	@Column(name = "actor_id", nullable = false)
	private short actorId;

	@Column(name = "first_name", nullable = false, length = 45)
	private String firstName;

	@Column(name = "last_name", nullable = false, length = 45)
	private String lastName;

	@Column(name = "film_info", length = 65535)
	private String filmInfo;

	public ActorInfoId() {
	}

	public ActorInfoId(short actorId, String firstName, String lastName) {
		this.actorId = actorId;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public ActorInfoId(short actorId, String firstName, String lastName,
			String filmInfo) {
		this.actorId = actorId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.filmInfo = filmInfo;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ActorInfoId))
			return false;
		ActorInfoId castOther = (ActorInfoId) other;

		return (this.getActorId() == castOther.getActorId())
				&& ((this.getFirstName() == castOther.getFirstName()) || (this
						.getFirstName() != null
						&& castOther.getFirstName() != null && this
						.getFirstName().equals(castOther.getFirstName())))
				&& ((this.getLastName() == castOther.getLastName()) || (this
						.getLastName() != null
						&& castOther.getLastName() != null && this
						.getLastName().equals(castOther.getLastName())))
				&& ((this.getFilmInfo() == castOther.getFilmInfo()) || (this
						.getFilmInfo() != null
						&& castOther.getFilmInfo() != null && this
						.getFilmInfo().equals(castOther.getFilmInfo())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getActorId();
		result = 37 * result
				+ (getFirstName() == null ? 0 : this.getFirstName().hashCode());
		result = 37 * result
				+ (getLastName() == null ? 0 : this.getLastName().hashCode());
		result = 37 * result
				+ (getFilmInfo() == null ? 0 : this.getFilmInfo().hashCode());
		return result;
	}
}
