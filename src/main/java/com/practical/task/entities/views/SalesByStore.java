package com.practical.task.entities.views;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "sales_by_store", catalog = "sakila")
public class SalesByStore implements java.io.Serializable {

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "store", column = @Column(name = "store", length = 101)),
		@AttributeOverride(name = "manager", column = @Column(name = "manager", length = 91)),
		@AttributeOverride(name = "totalSales", column = @Column(name = "total_sales", precision = 27)) })
	private SalesByStoreId id;

	public SalesByStore() {
	}

	public SalesByStore(SalesByStoreId id) {
		this.id = id;
	}
}
