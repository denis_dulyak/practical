package com.practical.task.entities.views;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "sales_by_film_category", catalog = "sakila")
public class SalesByFilmCategory implements java.io.Serializable {

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "category", column = @Column(name = "category", nullable = false, length = 25)),
		@AttributeOverride(name = "totalSales", column = @Column(name = "total_sales", precision = 27)) })
	private SalesByFilmCategoryId id;

	public SalesByFilmCategory() {
	}

	public SalesByFilmCategory(SalesByFilmCategoryId id) {
		this.id = id;
	}
}
