package com.practical.task.entities.views;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Getter
@Setter
@Embeddable
public class NicerButSlowerFilmListId implements java.io.Serializable {

	@Column(name = "FID")
	private Short fid;

	@Column(name = "title")
	private String title;

	@Column(name = "description", length = 65535)
	private String description;

	@Column(name = "category", nullable = false, length = 25)
	private String category;

	@Column(name = "price", precision = 4)
	private BigDecimal price;

	@Column(name = "length")
	private Short length;

	@Column(name = "rating", length = 5)
	private String rating;

	@Column(name = "actors", length = 65535)
	private String actors;

	public NicerButSlowerFilmListId() {
	}

	public NicerButSlowerFilmListId(String category) {
		this.category = category;
	}

	public NicerButSlowerFilmListId(Short fid, String title,
			String description, String category, BigDecimal price,
			Short length, String rating, String actors) {
		this.fid = fid;
		this.title = title;
		this.description = description;
		this.category = category;
		this.price = price;
		this.length = length;
		this.rating = rating;
		this.actors = actors;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof NicerButSlowerFilmListId))
			return false;
		NicerButSlowerFilmListId castOther = (NicerButSlowerFilmListId) other;

		return ((this.getFid() == castOther.getFid()) || (this.getFid() != null
				&& castOther.getFid() != null && this.getFid().equals(
				castOther.getFid())))
				&& ((this.getTitle() == castOther.getTitle()) || (this
						.getTitle() != null && castOther.getTitle() != null && this
						.getTitle().equals(castOther.getTitle())))
				&& ((this.getDescription() == castOther.getDescription()) || (this
						.getDescription() != null
						&& castOther.getDescription() != null && this
						.getDescription().equals(castOther.getDescription())))
				&& ((this.getCategory() == castOther.getCategory()) || (this
						.getCategory() != null
						&& castOther.getCategory() != null && this
						.getCategory().equals(castOther.getCategory())))
				&& ((this.getPrice() == castOther.getPrice()) || (this
						.getPrice() != null && castOther.getPrice() != null && this
						.getPrice().equals(castOther.getPrice())))
				&& ((this.getLength() == castOther.getLength()) || (this
						.getLength() != null && castOther.getLength() != null && this
						.getLength().equals(castOther.getLength())))
				&& ((this.getRating() == castOther.getRating()) || (this
						.getRating() != null && castOther.getRating() != null && this
						.getRating().equals(castOther.getRating())))
				&& ((this.getActors() == castOther.getActors()) || (this
						.getActors() != null && castOther.getActors() != null && this
						.getActors().equals(castOther.getActors())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getFid() == null ? 0 : this.getFid().hashCode());
		result = 37 * result + (getTitle() == null ? 0 : this.getTitle().hashCode());
		result = 37 * result + (getDescription() == null ? 0 : this.getDescription().hashCode());
		result = 37 * result + (getCategory() == null ? 0 : this.getCategory().hashCode());
		result = 37 * result + (getPrice() == null ? 0 : this.getPrice().hashCode());
		result = 37 * result + (getLength() == null ? 0 : this.getLength().hashCode());
		result = 37 * result + (getRating() == null ? 0 : this.getRating().hashCode());
		result = 37 * result + (getActors() == null ? 0 : this.getActors().hashCode());

		return result;
	}
}
