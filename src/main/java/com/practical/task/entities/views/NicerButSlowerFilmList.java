package com.practical.task.entities.views;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "nicer_but_slower_film_list", catalog = "sakila")
public class NicerButSlowerFilmList implements java.io.Serializable {

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "fid", column = @Column(name = "FID")),
		@AttributeOverride(name = "title", column = @Column(name = "title")),
		@AttributeOverride(name = "description", column = @Column(name = "description", length = 65535)),
		@AttributeOverride(name = "category", column = @Column(name = "category", nullable = false, length = 25)),
		@AttributeOverride(name = "price", column = @Column(name = "price", precision = 4)),
		@AttributeOverride(name = "length", column = @Column(name = "length")),
		@AttributeOverride(name = "rating", column = @Column(name = "rating", length = 5)),
		@AttributeOverride(name = "actors", column = @Column(name = "actors", length = 65535)) })
	private NicerButSlowerFilmListId id;

	public NicerButSlowerFilmList() {
	}

	public NicerButSlowerFilmList(NicerButSlowerFilmListId id) {
		this.id = id;
	}
}
