package com.practical.task.entities.views;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "actor_info")
public class ActorInfo implements Serializable {

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "actorId", column = @Column(name = "actor_id", nullable = false)),
		@AttributeOverride(name = "firstName", column = @Column(name = "first_name", nullable = false, length = 45)),
		@AttributeOverride(name = "lastName", column = @Column(name = "last_name", nullable = false, length = 45)),
		@AttributeOverride(name = "filmInfo", column = @Column(name = "film_info", length = 65535)) })
	private ActorInfoId id;

	public ActorInfo() {
	}

	public ActorInfo(ActorInfoId id) {
		this.id = id;
	}
}
