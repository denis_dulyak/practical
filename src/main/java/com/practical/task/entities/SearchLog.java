package com.practical.task.entities;

import com.practical.task.beans.SearchValueBean;
import com.practical.task.utils.SearchValueConverter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Denis Dulyak on 02.11.2016.
 */
@Getter
@Setter
@Entity
@Table(name = "search_log")
public class SearchLog implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "fb_user_id", nullable = false)
    private String fbUserId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", nullable = false)
    private Date createDate;

    @Column(name = "count")
    private Integer count;

    @Convert(converter = SearchValueConverter.class)
    @Column(name = "search_value")
    private SearchValueBean searchValue;
}
