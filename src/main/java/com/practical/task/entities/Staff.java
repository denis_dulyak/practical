package com.practical.task.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Table(name = "staff", catalog = "sakila")
public class Staff implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "staff_id", unique = true, nullable = false)
	private Byte staffId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "address_id", nullable = false)
	private Address address;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "store_id", nullable = false)
	private Store store;

	@Column(name = "first_name", nullable = false, length = 45)
	private String firstName;

	@Column(name = "last_name", nullable = false, length = 45)
	private String lastName;

	@Column(name = "picture")
	private byte[] picture;

	@Column(name = "email", length = 50)
	private String email;

	@Column(name = "active", nullable = false)
	private boolean active;

	@Column(name = "username", nullable = false, length = 16)
	private String username;

	@Column(name = "password", length = 40)
	private String password;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update", nullable = false, length = 19)
	private Date lastUpdate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "staff")
	private List<Payment> payments;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "staff")
	private List<Store> stores;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "staff")
	private List<Rental> rentals;
}
