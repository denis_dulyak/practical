package com.practical.task.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "film_text", catalog = "sakila")
public class FilmText implements java.io.Serializable {

	@Id
	@Column(name = "film_id", unique = true, nullable = false)
	private short filmId;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "description", length = 65535)
	private String description;
}
