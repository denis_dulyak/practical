package com.practical.task.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Setter
@Embeddable
public class FilmActorId implements java.io.Serializable {

    @Column(name = "actor_id", nullable = false)
    private short actorId;

    @Column(name = "film_id", nullable = false)
    private short filmId;

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof FilmActorId))
            return false;
        FilmActorId castOther = (FilmActorId) other;

        return (this.getActorId() == castOther.getActorId())
            && (this.getFilmId() == castOther.getFilmId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getActorId();
        result = 37 * result + this.getFilmId();
        return result;
    }

}
