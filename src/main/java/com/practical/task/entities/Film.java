package com.practical.task.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Table(name = "film", catalog = "sakila")
public class Film implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "film_id", unique = true, nullable = false)
	private Short filmId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "language_id", nullable = false)
	private Language languageByLanguageId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "original_language_id")
	private Language languageByOriginalLanguageId;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "description", length = 65535)
	private String description;

	@Temporal(TemporalType.DATE)
	@Column(name = "release_year")
	private Date releaseYear;

	@Column(name = "rental_duration", nullable = false)
	private byte rentalDuration;

	@Column(name = "rental_rate", nullable = false, precision = 4)
	private BigDecimal rentalRate;

	@Column(name = "length")
	private Short length;

	@Column(name = "replacement_cost", nullable = false, precision = 5)
	private BigDecimal replacementCost;

	@Column(name = "rating")
	private String rating;

	@Column(name = "special_features")
	private String specialFeatures;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update", nullable = false)
	private Date lastUpdate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "film")
	private List<Inventory> inventories;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "film")
	private List<FilmActor> filmActors;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "film")
	private List<FilmCategory> filmCategories;
}
