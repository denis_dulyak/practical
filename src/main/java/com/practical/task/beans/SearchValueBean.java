package com.practical.task.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Denis Dulyak on 02.11.2016.
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchValueBean {

    private String title;
    private String description;
    private String category;
    private String languageName;
    private String actor;
}
